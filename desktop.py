#! /usr/bin/env python

# Copyright 2011 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

from PySide.QtCore import *
from PySide.QtGui import *

import synchronal_lib as lib
dbg, dbg_msg = lib.dbg, lib.dbg_msg

import sys

class DesktopMainWindow(lib.MainWindow):
    @dbg
    def do_layout(self):
        main_widget = QWidget()
        self.setCentralWidget(main_widget)

        v_layout = QVBoxLayout()
        main_widget.setLayout(v_layout)

        self.send_box = lib.Tweeter(self.api, self)
        v_layout.addWidget(self.send_box)

        self.stream_layout = QSplitter()
        v_layout.addWidget(self.stream_layout)

    @dbg
    def do_initial_streams(self):
        lib.MainWindow.do_initial_streams(self)
        state = QSettings().value("splitter")
        if state:
            self.stream_layout.restoreState(state)

    @dbg
    def closeEvent(self, event):
        QSettings().setValue("splitter", self.stream_layout.saveState())
        lib.MainWindow.closeEvent(self, event)


synchronal = lib.SynchronalApplication(DesktopMainWindow, sys.argv)

synchronal.exec_()
