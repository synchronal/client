from datetime import datetime
from PySide.QtCore import Slot, Signal, QObject, QThread
from PySide.QtGui import QTextBrowser

debug_enabled = True

_debug = None
def debug():
    global _debug
    if _debug is None:
        _debug = Debug()
    return _debug

_debug_widget = None
def debug_widget():
    global _debug_widget
    if _debug_widget is None:
        _debug_widget = Debug_Widget()
    else:
        _debug_widget.show()

class Debug(QObject):
    current_indent = 0
    threadcount = 0
    threads = {}
    thread_debug_enabled = True
    log = []
    updated = Signal(str)

    def dbg_msg(self, *args, **kwargs):
        # kwargs is only used to allow indent to be enabled or disabled
        # other kwargs are IGNORED
        if debug_enabled:
            indent = self.current_indent * " "
            if indent in kwargs:
                if not kwargs["indent"]:
                    indent = ""
            msg = datetime.now().strftime("[%H:%M:%S]") + indent +  " ".join(str(i) for i in args)
            try:
                print msg
            except IOError:
                # this happens occasionally on Windows
                pass
            self.updated.emit(msg)
            self.log.append(msg)

    def dbg_ts(self, id, *args):
        if self.thread_debug_enabled:
            tc = str(self.threadcount)
            self.threads[id] = tc
            self.threadcount += 1
            self.dbg_msg("THREAD{} START:".format(tc), *args, indent=False)

    def dbg_t(self, id, *args):
        if self.thread_debug_enabled:
            self.dbg_msg("THREAD{} INFO_:".format(self.threads[id]), *args, indent=False)

    def dbg_te(self, id):
        if self.thread_debug_enabled:
            self.dbg_msg("THREAD{} END".format(self.threads[id]), indent=False)
            # no need to keep references to finished threads around here
            del self.threads[id]

    @Slot(str, QThread, tuple)
    def thread_debug(self, status, id, args):
        if status == "start":
            self.dbg_ts(id, *args)
        elif status == "end":
            self.dbg_te(id)
        else:
            self.dbg_t(id, *args)

    def dbg(self, fn):
        def wrapper(*args, **kwargs):
            fnname = fn.__name__
            try:
                classname = args[0].__class__.__name__ + "."
            except AttributeError:
                classname = ""
            self.dbg_msg("START:{}{}".format(classname, fnname))
            self.current_indent += 1
            res = fn(*args, **kwargs)
            self.current_indent -= 1
            self.dbg_msg("END__:{}{}".format(classname, fnname))
            return res
        return wrapper

class Debug_Widget(QTextBrowser):
    def __init__(self, *args):
        QTextBrowser.__init__(self, *args)
        log = debug()
        self.setWindowTitle("Synchronal - Debug")
        self.setLineWrapMode(QTextBrowser.NoWrap)
        self.setFontFamily("mono")
        self.append("\n".join(log.log))
        log.updated.connect(self.append)
        self.show()
