#! /usr/bin/env python

# Copyright 2011 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

# NOTE
# QDesktopServices.openUrl appears to be non-functional in Android Qt at the
# moment.

import cgitb, sys
cgitb.enable(False, "/sdcard/pysidelog/", format="txt")
sys.path.append("/sdcard")

from PySide.QtCore import *
from PySide.QtGui import *

import synchronal_lib as lib
dbg, dbg_msg = lib.dbg, lib.dbg_msg

class MobileMainWindow(lib.MainWindow):
    @dbg
    def do_layout(self):
        main_widget = QWidget()
        self.setCentralWidget(main_widget)

        v_layout = QVBoxLayout()
        main_widget.setLayout(v_layout)

        self.send_box = lib.Tweeter(self.api, self)
        v_layout.addWidget(self.send_box)

        self.stream_layout = QStackedLayout()
        v_layout.addLayout(self.stream_layout)


synchronal = lib.SynchronalApplication(MobileMainWindow, sys.argv)

synchronal.exec_()
