#! /usr/bin/env python
# convenience script to pre-render PNGs for devices without SVG support
import os
from PySide.QtGui import QIcon, QPixmap, QApplication

app = QApplication("")
icon = QIcon("resources/synchronal_logo.svg")
for size in (16, 32, 64, 200):
    pix = icon.pixmap(size, size)
    path = "resources/synchronal_logo_{}.png".format(size)
    pix.save(path, "png")
