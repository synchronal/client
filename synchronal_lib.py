# Copyright 2011 Sam Lade
#
# This file is part of Synchronal.
#
# Synchronal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Synchronal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Synchronal.  If not, see <http://www.gnu.org/licenses/>.

from PySide.QtCore import *
from PySide.QtGui import *

import debug
d = debug.debug()
dbg, dbg_msg = d.dbg, d.dbg_msg

import tweepy, json

import os, os.path, urllib, urllib2, hashlib, htmlentitydefs, re, collections, platform

try: import setproctitle
except ImportError: pass
else: setproctitle.setproctitle("synchronal")

__version__ = "0.0.0"

class SynchronalApplication(QApplication):
    @dbg
    def __init__(self, main_window, *args):
        QApplication.__init__(self, *args)
        self.MainWindow = main_window
        self.setOrganizationName("Sentynel")
        self.setApplicationName("Synchronal")
        self.setQuitOnLastWindowClosed(False)

        datadir = QDesktopServices.storageLocation(QDesktopServices.DataLocation)
        if not os.path.exists(datadir):
            QDir().mkpath(datadir)

        # install icons
        try:
            files = os.listdir("resources")
        except OSError:
            pass
        else:
            for copy in files:
                if copy.endswith((".png", ".svg")):
                    iconpath = os.path.join(datadir, copy)
                    try:
                        logo = open(os.path.join("resources", copy), "rb")
                    except IOError:
                        pass
                    else:
                        # copy to data location
                        try:
                            with open(iconpath, "wb") as f:
                                f.write(logo.read())
                        except IOError:
                            pass
                        logo.close()

        # load icons
        icon = QIcon()
        files = os.listdir(datadir)
        if ("svg" in QImageReader.supportedImageFormats()) & ("synchronal_logo.svg" in files):
            ext = ".svg"
        else:
            ext = ".png"
        for i in files:
            if re.match(r"synchronal_logo\w*\{}".format(ext), i):
                icon.addFile(os.path.join(datadir, i))
        self.setWindowIcon(icon)

        # set user agent string
        opener = urllib2.build_opener()
        opener.addheaders = [("User-agent", "Synchronal Client " + __version__ + " on " + platform.platform())]
        urllib2.install_opener(opener)

        auth = AuthRequester()
        auth.done.connect(self.check_auth)
        auth.get_auth()

    @dbg
    def check_auth(self, auth):
        if auth == -1:
            dbg_msg("Not authed, exiting")
            self.quit()
            return
        self.main_win = self.MainWindow(auth, self)


class Tweet(QWidget):
    """A widget displaying and holding the values for a single tweet."""
    def __init__(self, tweet, img_requester, stream, *args):
        QWidget.__init__(self, *args)
        self.tweet = tweet
        self.img_requester = img_requester
        self.stream = stream
        # construct parameters from the tweet object returned by API
        # first check if it's a retweet
        self.tid = tweet.id_str
        if "retweeted_status" in tweet.__dict__:
            self.user = tweet.retweeted_status.user.screen_name
            self.name = tweet.retweeted_status.user.name
            self.msg = tweet.retweeted_status.text
            self.time = self.timestamp_to_str(tweet.retweeted_status.created_at)
            self.img_url = tweet.retweeted_status.user.profile_image_url_https
            self.retweeter = tweet.user.screen_name
            self.entities = tweet.retweeted_status.entities
            self.in_reply = tweet.retweeted_status.in_reply_to_status_id_str
        else:
            self.user = tweet.user.screen_name
            self.name = tweet.user.name
            self.msg = tweet.text
            self.time = self.timestamp_to_str(tweet.created_at)
            self.img_url = tweet.user.profile_image_url_https
            self.retweeter = None
            self.entities = tweet.entities
            self.in_reply = tweet.in_reply_to_status_id_str

        if "current_user_retweet" in tweet.__dict__:
            self.retweeted = True
        else:
            self.retweeted = False

        self.photo = None
        self.msg = self.handle_entities(self.entities, self.msg)

        self.do_layout()

    def handle_entity(self, entity, fragment, fragments_list, fragments):
        index = entity["indices"]
        indexstr = str(index[0]) + ":" + str(index[1])
        fragments_list.append(index)
        fragments[indexstr] = fragment

    def handle_entities(self, entities, msg):
        fragments = {}
        fragments_list = []
        for url in entities["urls"]:
            fragment = u'<a href="{0}">{1}</a>'
            if "display_url" in url:
                fragment = fragment.format(url["expanded_url"], url["display_url"])
            else:
                u = url["url"]
                if not u.startswith(("http://", "https://")):
                    u = "http://" + u
                fragment = fragment.format(u, url["url"])

            self.handle_entity(url, fragment, fragments_list, fragments)

        for user in entities["user_mentions"]:
            fragment = u'<a href="@{0}">@{0}</a>'.format(user["screen_name"])
            self.handle_entity(user, fragment, fragments_list, fragments)

        # media entity isn't guaranteed to be present apparently
        if "media" in entities:
            for media in entities["media"]:
                if media["type"] != "photo":
                    # only photo available at time of writing but handle potential
                    # upgrades for twitter
                    continue
                if self.photo:
                    # we only support one media entity per tweet at the moment
                    # print a warning if more are found and ignore them
                    dbg_msg("Warning: Multiple media entities encountered for tweet", self.tid)
                    continue
                self.photo = media
                # for the moment we just handle this like regular URLs and show the photo
                # url in the text, with the image available on mouseover, but there ought
                # to be a better way to make clear that there's an image available
                fragment = u'<a href="{0}">{1}</a>'
                if "display_url" in media:
                    fragment = fragment.format(media["expanded_url"], media["display_url"])
                else:
                    fragment = fragment.format(url["url"], url["url"])

                self.handle_entity(media, fragment, fragments_list, fragments)

        fragments_list.sort()
        msglist = []
        for i in range(len(fragments_list)):
            # first get plaintext fragment preceeding this entity
            if i == 0:
                msglist.append(msg[:fragments_list[i][0]])
            else:
                msglist.append(msg[fragments_list[i-1][1]:fragments_list[i][0]])

            # now get this entity
            msglist.append(fragments[str(fragments_list[i][0]) + ":" + str(fragments_list[i][1])])
        # get final fragment
        if fragments_list:
            msglist.append(msg[fragments_list[-1][1]:])
        else:
            msglist.append(msg)

        msg = "".join(msglist)
        return msg

    def enterEvent(self, event):
        self.extras.show()
        event.accept()

    def leaveEvent(self, event):
        self.extras.hide()
        event.accept()

    def timestamp_to_str(self, stamp):
        #TODO provide i18n for the format here
        # timezone handling in Python is insane and silly, and Python datetime ->
        # QDateTime is buggy, so here's a convoluted mess instead.
        date = QDate(stamp.date())
        t = stamp.time()
        time = QTime(t.hour, t.minute, t.second, t.microsecond/1000)
        datetime = QDateTime(date, time, Qt.UTC)
        return datetime.toLocalTime().toString("hh:mm:ss dd/MM/yy")

    def do_layout(self):
        # twitter avatar size
        avatar_size = 48

        outerlayout = QVBoxLayout()

        user_label = QLabel()
        # don't want usernames to be blue and underlined, so get the standard
        # text color from the palette and set that
        color = user_label.palette().text().color().name()
        style = "color: {0}; text-decoration: none;".format(color)
        self.plain_url_style = style
        user_text = u"<strong><a style='{2}' href='@{0}'>{0}</a></strong> ({1})".format(self.user, self.name, style)
        user_label.setText(user_text)
        user_label.linkActivated.connect(self.handle_url)
        outerlayout.addWidget(user_label)

        if self.retweeter:
            retweeter_layout = QHBoxLayout()
            rt_label = QLabel()
            rt_label.setPixmap(QPixmap("resources/retweet.png"))
            rt_label.setMaximumWidth(16)
            retweeter_layout.addWidget(rt_label)
            rter_label = QLabel("by " + self.retweeter)
            retweeter_layout.addWidget(rter_label)
            outerlayout.addLayout(retweeter_layout)

        hlayout = QHBoxLayout()
        outerlayout.addLayout(hlayout)

        self.img_label = QLabel()
        self.img_label.setMaximumWidth(avatar_size)
        self.img_label.setMinimumWidth(avatar_size)
        self.img_label.setMaximumHeight(avatar_size)
        self.img_label.setMinimumHeight(avatar_size)
        self.img_requester.get_image(self.img_url, self.set_img)
        hlayout.addWidget(self.img_label)

        msg_label = Wrapping_Label()
        msg_label.setTextInteractionFlags(Qt.LinksAccessibleByMouse|Qt.TextSelectableByMouse)
        msg_label.setTextFormat(Qt.RichText)
        msg_label.setText(self.msg)
        msg_label.linkActivated.connect(self.handle_url)
        hlayout.addWidget(msg_label)

        time_label = QLabel(u"<a href='https://www.twitter.com/{0}/status/{1}' style='{2}'>{3}</a>".format(self.user, self.tid, style, self.time))
        time_label.linkActivated.connect(self.handle_url)
        outerlayout.addWidget(time_label)

        self.extras = QWidget()
        extras_layout = QVBoxLayout()
        self.extras.setLayout(extras_layout)
        outerlayout.addWidget(self.extras)
        self.extras.hide()

        if self.photo:
            self.media_label = QLabel("Loading media...")
            # at some point we might want to do a cleverer selection of size here
            # based on the amount of size actually available, but for now just get
            # the thumbnail
            self.img_requester.get_image(self.photo["media_url_https"] + ":thumb", self.got_media)
            extras_layout.addWidget(self.media_label)

        if self.in_reply:
            self.get_history_button = QPushButton("Load history")
            self.get_history_button.setFlat(True)
            self.get_history_button.clicked.connect(self.get_history)
            extras_layout.addWidget(self.get_history_button)

        extras_buttons_layout = QHBoxLayout()
        extras_layout.addLayout(extras_buttons_layout)
        if self.retweeted:
            rt_pixmap = QPixmap("resources/retweet_on.png")
        else:
            rt_pixmap = QPixmap("resources/retweet.png")
        self.retweet_button = QPushButton(rt_pixmap, "Retweet")
        self.retweet_button.setFlat(True)
        if self.retweeted:
            #TODO make this an un-retweet button instead in this case
            self.retweet_button.setEnabled(False)
            self.retweet_button.setText("Retweeted")
        self.retweet_button.clicked.connect(self.retweet)
        extras_buttons_layout.addWidget(self.retweet_button)

        reply_button = QPushButton(QPixmap("resources/reply.png"), "Reply")
        reply_button.setFlat(True)
        reply_button.clicked.connect(self.reply)
        extras_buttons_layout.addWidget(reply_button)

        # add a separator
        sep = QFrame()
        sep.setFrameShape(QFrame.HLine)
        outerlayout.addWidget(sep)

        self.setLayout(outerlayout)

    def get_history(self):
        self.get_history_button.setEnabled(False)
        self.history_request = RequestThread(self.got_history, self.stream.main_win.api.get_status, {"id":self.in_reply, "include_entities":True})

    def got_history(self, hist):
        if hist == -1:
            self.get_history_button.setEnabled(True)
            return

        msg = self.handle_entities(hist.entities, hist.text)
        hist_text = u"<strong><a href='@{0}' style='{1}'>{0}</a></strong><br />{2}".format(hist.user.screen_name, self.plain_url_style, msg)

        hist_label = QLabel()
        hist_label.setWordWrap(True)
        hist_label.setTextInteractionFlags(Qt.LinksAccessibleByMouse|Qt.TextSelectableByMouse)
        hist_label.setTextFormat(Qt.RichText)
        hist_label.setText(hist_text)
        hist_label.linkActivated.connect(self.handle_url)
        pos = self.extras.layout().count() - 2
        self.extras.layout().insertWidget(pos, hist_label)

        if hist.in_reply_to_status_id_str:
            self.in_reply = hist.in_reply_to_status_id_str
            self.get_history_button.setEnabled(True)
        else:
            self.get_history_button.hide()

    def got_media(self, f):
        if f[0] == "~":
            # request failed, retry at some point?
            self.media_label.setText("Image request failed.")
            return
        self.media_label.setPixmap(QPixmap(f))

    def handle_url(self, url):
        if url[0] == "@":
            # username reference
            self.stream.main_win.add_stream(url)
        else:
            QDesktopServices.openUrl(QUrl(urllib.unquote(url)))

    def retweet(self):
        self.retweet_button.setEnabled(False)
        RequestThread(self.done_retweet, self.tweet.retweet, None, self)

    def done_retweet(self, res):
        if res != -1:
            self.retweet_button.setIcon(QPixmap("resources/retweet_on.png"))
            self.retweet_button.setText("Retweeted")
        else:
            self.retweet_button.setEnabled(True)
            dbg_msg("Retweet failed")

    def reply(self):
        send_box = self.stream.main_win.send_box
        send_box.send_box.setText(u"@{0} ".format(self.user))
        send_box.set_reply(self.tid)
        send_box.send_box.setFocus()

    def set_img(self, f):
        if f[0] == "~":
            # request failed, retry at some point?
            return
        # scale to the size the small profile images are supposed to be
        # because apparently, they're not guaranteed to be the right size.
        self.img_label.setPixmap(QPixmap(f).scaled(48,48, Qt.KeepAspectRatio,
                                 Qt.SmoothTransformation))


class Stream(QWidget):
    """A widget holding a stream of Tweets.

    This can be an individual user's stream, the user's home stream, etc.
    Needs to be given a name for the stream (to use with Synchronal server),
    and a function with arguments for retrieving the stream in question."""
    @dbg
    def __init__(self, stream_name, req_fn, req_fn_kwargs, img_requester, synch, main_win, *args):
        QWidget.__init__(self, *args)
        self.stream_name = stream_name
        self.req_fn = req_fn
        self.req_fn_kwargs = req_fn_kwargs
        self.img_requester = img_requester
        self.synch = synch
        self.main_win = main_win
        self.datadir = QDesktopServices.storageLocation(QDesktopServices.DataLocation)
        self.last_read = "0"
        self.reading = "0"
        self.last_fetched = "0"
        self.oldest = "0"
        self.initial_tid = "0"
        self.tweets = collections.deque()
        self.top = None
        self.updating = False
        self.t_get_tweets = None

        self.set_tid_timer = QTimer()
        self.set_tid_timer.setSingleShot(True)
        self.set_tid_timer.timeout.connect(self.set_tid)

        self.new_tweet_timer = QTimer()
        self.new_tweet_timer.timeout.connect(self.get_tweets)
        self.new_tweet_timer.setInterval(60*1000)

        self.do_layout()
        self.synch.get_read(self.stream_name, self.got_tid)
        if self.stream_name[0] == "@":
            self.profile_request = RequestThread(self.show_user, main_win.api.get_user,
                {"screen_name":self.stream_name[1:]})

    @dbg
    def got_tid(self, tid, x):
        if isinstance(tid, (unicode, str)):
            self.last_fetched = tid
            self.initial_tid = tid
            if self.oldest == "0":
                self.oldest = tid
            self.get_tweets(tid)
        else:
            # no available saved position
            dbg_msg("No", self.stream_name, "saved position available")
            self.get_tweets()
        self.old_tweets_button.setEnabled(True)
        self.updating = True
        if self.refresh_box.currentText() != "--":
            self.new_tweet_timer.start()

    @dbg
    def set_tid(self):
        self.set_tid_request = self.synch.set_read(self.stream_name, self.check_set_tid, self.last_read)

    def check_set_tid(self, code, x):
        if code != 0:
            dbg_msg("Setting read TID on server failed:", code)

    def do_layout(self):
        outer_layout = QVBoxLayout()
        self.outer_layout = outer_layout
        self.setLayout(outer_layout)

        header_layout = QHBoxLayout()
        outer_layout.addLayout(header_layout)

        header_layout.addWidget(QLabel(u'<strong>{0}</strong>'.format(self.stream_name)))

        read_button = QToolButton()
        read_button.setAutoRaise(True)
        read_button.setIcon(
            QIcon.fromTheme("news-subscribe",
            QIcon(os.path.join(self.datadir, "news-subscribe.png"))))
        read_button.clicked.connect(self.mark_read)
        read_button.setToolTip("Mark tweets read")
        header_layout.addWidget(read_button)

        refresh_button = QToolButton()
        refresh_button.setAutoRaise(True)
        refresh_button.setIcon(
            QIcon.fromTheme("view-refresh",
            QIcon(os.path.join(self.datadir, "view-refresh.png"))))
        refresh_button.clicked.connect(self.get_tweets)
        refresh_button.setToolTip("Get new tweets")
        header_layout.addWidget(refresh_button)

        self.refresh_box = QComboBox()
        self.refresh_box.addItems(["1m", "2m", "5m", "10m", "30m", "1h", "--"])
        self.refresh_box.setToolTip("Auto refresh frequency")
        p = self.refresh_box.sizePolicy()
        p.setHorizontalPolicy(QSizePolicy.Fixed)
        self.refresh_box.setSizePolicy(p)
        self.refresh_box.currentIndexChanged[unicode].connect(self.set_update_f)
        header_layout.addWidget(self.refresh_box)

        close_button = QToolButton()
        close_button.setAutoRaise(True)
        close_button.setIcon(
            QIcon.fromTheme("window-close",
            QIcon(os.path.join(self.datadir, "window-close.png"))))
        close_button.clicked.connect(self.remove_stream)
        close_button.setToolTip("Close stream")
        header_layout.addWidget(close_button)

        self.scroll_area = QScrollArea()
        outer_layout.addWidget(self.scroll_area)

        self.widget = QWidget()
        self.scroll_area.setWidget(self.widget)
        self.layout = QVBoxLayout()

        # to avoid visual glitches on hover over tweets, the necessary spacing
        # goes in the tweets themselves
        self.layout.setSpacing(0)
        self.widget.setLayout(self.layout)
        # add stretch so tweets aren't stretched to fit the column height if it's
        # too tall
        self.layout.addStretch()

        self.old_tweets_button = QPushButton("Load older tweets")
        self.old_tweets_button.clicked.connect(self.get_old_tweets)
        self.old_tweets_button.setEnabled(False)
        self.layout.insertWidget(0, self.old_tweets_button)

        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.min_width = 0

        self.scroll_area.verticalScrollBar().valueChanged.connect(self.check_pos)
        self.scroll_area.verticalScrollBar().rangeChanged.connect(self.resized)

    @dbg
    def show_user(self, user):
        self.profile_request = None
        if user == -1:
            return

        self.user = user

        user_layout = QHBoxLayout()
        self.large_img_label = QLabel()
        self.large_img_label.setMaximumWidth(128)
        img_url = re.sub(
            r"normal\.[a-zA-Z]{3}$",
            lambda x: "reasonably_small."+x.group().split(".")[1],
            user.profile_image_url_https)
        self.img_requester.get_image(img_url, self.set_img)
        user_layout.addWidget(self.large_img_label)

        user_details_layout = QVBoxLayout()
        user_layout.addLayout(user_details_layout)
        name_label = QLabel(u"<strong>{0}</strong>".format(user.name))
        user_details_layout.addWidget(name_label)
        desc_layout =QLabel(user.description)
        desc_layout.setWordWrap(True)
        user_details_layout.addWidget(desc_layout)
        url_label = QLabel(u"<a href='{0}'>{0}</a>".format(user.url))
        url_label.linkActivated.connect(self.handle_url)
        user_details_layout.addWidget(url_label)

        self.follow_button = QPushButton()
        self.user_button_state(user.following)
        self.follow_button.clicked.connect(self.user_do_follow)
        user_details_layout.addWidget(self.follow_button)

        self.outer_layout.insertLayout(1,user_layout)

    def user_button_state(self, following):
        if following:
            self.follow_button.setText("Following")
            self.follow_button.setIcon(QIcon.fromTheme("task-complete",
                QIcon(os.path.join(self.datadir, "task-complete.png"))))
        else:
            self.follow_button.setText("Follow")
            self.follow_button.setIcon(QIcon.fromTheme("list-add",
                QIcon(os.path.join(self.datadir, "list-add.png"))))

    @dbg
    def user_do_follow(self):
        self.follow_button.setEnabled(False)
        if not self.user.following:
            self.user_follow_request = RequestThread(self.user_done_follow,
                self.main_win.api.create_friendship, {"user_id":self.user.id})
        else:
            self.user_follow_request = RequestThread(self.user_done_follow,
                self.main_win.api.destroy_friendship, {"user_id":self.user.id})

    @dbg
    def user_done_follow(self, res):
        self.follow_button.setEnabled(True)
        if res == -1:
            dbg_msg("Follow/unfollow request failed")
            return
        self.user.following = not self.user.following
        self.user_button_state(self.user.following)

    def add_tweet(self, tweet, top=True):
        # for scroll handling
        if not self.tweets:
            self.top = tweet
        if top:
            insertion_point = 0
            self.tweets.appendleft(tweet)
        else:
            insertion_point = self.layout.count() - 2
            self.tweets.append(tweet)

        self.layout.insertWidget(insertion_point, tweet)
        if self.oldest == "0":
            self.oldest = str(int(tweet.tid) - 1)
        if top:
            self.last_fetched = tweet.tid
        else:
            self.oldest = str(int(tweet.tid) - 1)

        # kludge to avoid shrinking too far and words escaping off the side
        # 35 is an arbitrary padding to avoid clashes with scroll bars etc
        if tweet.minimumSizeHint().width() + 35 > self.min_width:
            self.min_width = tweet.minimumSizeHint().width() + 35
            self.scroll_area.setMinimumWidth(self.min_width)

    def resized(self, start, stop):
        newtop = self.tweets[0]
        if newtop != self.top:
            if self.top:
                self.scroll_area.verticalScrollBar().setValue(
                    self.scroll_area.verticalScrollBar().value() +
                    (self.top.y() - newtop.y()))
            self.top = newtop

    def check_pos(self, pos):
        if pos == 0:
            # if scrolled all the way to the top, mark top tweet as read
            if self.reading != "0":
                if self.last_read != self.reading:
                    self.last_read = self.reading
                    self.set_tid_timer.start(0)
                return

        at = self.tweet_at(pos)
        if not at:
            return

        tid = at.tid
        if int(tid) > int(self.reading):
            self.last_read, self.reading = self.reading, tid
            self.set_tid_timer.start(30*1000)

    def tweet_at(self, pos):
        at = self.widget.childAt(10, pos)
        if not isinstance(at, Tweet):
            if isinstance(at, (QLabel, QFrame)):
                if isinstance(at.parent(), Tweet):
                    at = at.parent()
                else:
                    return None
            else:
                return None
        return at

    def set_update_f(self, f):
        if f == "--":
            self.new_tweet_timer.stop()
        elif f[-1] == "h":
            self.new_tweet_timer.setInterval(60 * 60 * 1000 * int(f[:-1]))
            if self.updating:
                self.new_tweet_timer.start()
        elif f[-1] == "m":
            self.new_tweet_timer.setInterval(60 * 1000 * int(f[:-1]))
            if self.updating:
                self.new_tweet_timer.start()
        else:
            dbg_msg("Unrecognised new_tweet_timer frequency set")

    def remove_stream(self):
        # remove stored read status on stream removal
        self.set_tid_request = self.synch.set_read(self.stream_name, self.deleteLater, "0")
        self.main_win.remove_stream(self)
        self.new_tweet_timer.stop()
        self.set_tid_timer.stop()

    def mark_read(self):
        self.last_read = self.last_fetched
        self.set_tid()

    def handle_url(self, url):
        QDesktopServices.openUrl(QUrl(urllib.unquote(url)))

    def set_img(self, f):
        if f[0] == "~":
            # request failed, retry at some point?
            return
        self.large_img_label.setPixmap(QPixmap(f).scaled(128,128, Qt.KeepAspectRatio,
                                       Qt.SmoothTransformation))

    @dbg
    def get_tweets(self, from_tid=None):
        args = self.req_fn_kwargs.copy()
        if (not isinstance(from_tid, str)) | (from_tid == "0"):
            from_tid = None
        if from_tid != None:
            args["since_id"] = from_tid
            args["count"] = 200
            dbg_msg("Getting", self.stream_name, "tweets since", from_tid)
        elif self.last_fetched != "0":
            args["since_id"] = self.last_fetched
            args["count"] = 200
            dbg_msg("Getting", self.stream_name, "tweets since", self.last_fetched)
        else:
            args["count"] = 20
            dbg_msg("Getting", self.stream_name, "most recent tweets")
        if self.t_get_tweets:
            # request currently running
            if self.t_get_tweets.is_finished:
                dbg_msg("Tweet request is already running; has finished but not been cleaned up")
            else:
                dbg_msg("Tweet request is already running; scheduling new request afterwards.")
                self.t_get_tweets.finished.connect(self.get_tweets_wrap)
        else:
            self.t_get_tweets = RequestThread(self.new_tweets, self.req_fn, args)

    def get_tweets_wrap(self):
        self.get_tweets()

    @dbg
    def get_old_tweets(self):
        if self.oldest == "0":
            return
        args = self.req_fn_kwargs.copy()
        args["count"] = 20
        args["max_id"] = self.oldest
        self.t_get_old_tweets = RequestThread(self.old_tweets, self.req_fn, args)
        self.old_tweets_button.setEnabled(False)

    @dbg
    def get_more_new_tweets(self):
        args = self.req_fn_kwargs.copy()
        args["max_id"] = self.oldest
        args["since_id"] = self.initial_tid
        args["count"] = 200
        # note: use old_tweets here so stuff is added to the bottom
        self.t_get_tweets = RequestThread(self.old_tweets, self.req_fn, args)

    @dbg
    def old_tweets(self, tweets):
        self.t_get_old_tweets = None
        self.old_tweets_button.setEnabled(True)
        if tweets == -1:
            return
        dbg_msg("Adding", len(tweets), "old tweets to", self.stream_name)
        for i in tweets:
            self.add_tweet(Tweet(i, self.img_requester, self), False)

        if len(tweets) > 100:
            # this will never be the case for a regular old tweets request
            # only for a continuance
            self.get_more_new_tweets()

    @dbg
    def new_tweets(self, tweets):
        self.t_get_tweets = None
        if tweets == -1:
            # request failed for some reason
            #TODO implement retry etc
            return
        dbg_msg("Adding", len(tweets), "tweets to", self.stream_name)
        # iterate in reverse order so new tweets -> top
        for i in reversed(tweets):
            self.add_tweet(Tweet(i, self.img_requester, self))
        if len(tweets) > 0:
            QApplication.alert(self)

        if len(tweets) > 100:
            # there might be more unread tweets
            # theoretically twitter returns 200, but this seems to be unreliable
            # so allow a margin for error
            self.get_more_new_tweets()


class Tweeter(QWidget):
    """QLineEdit for composing and posting tweets.

    Provides a completer for usernames."""
    @dbg
    def __init__(self, api, main_win, *args):
        QWidget.__init__(self, *args)

        policy = self.sizePolicy()
        policy.setVerticalPolicy(QSizePolicy.Fixed)
        self.setSizePolicy(policy)

        layout = QHBoxLayout()
        self.setLayout(layout)
        self.send_box = QLineEdit()
        layout.addWidget(self.send_box)
        self.char_count_label = QLabel("0")
        layout.addWidget(self.char_count_label)
        self.char_count = 0
        self.send_box.setPlaceholderText("Tweet")

        self.api = api
        self.main_win = main_win
        self.reply_to = None
        self.followed_requester = FollowedRequester(self.got_followed, self.api)
        self.send_box.returnPressed.connect(self.post_tweet)
        self.send_box.textChanged.connect(self.set_len)

    @dbg
    def post_tweet(self):
        if self.char_count > 140:
            QMessageBox.warning(self, "Synchronal", "Your tweet is too long and cannot be posted.")
            return
        params = {}
        if self.reply_to:
            params["in_reply_to_status_id"] = self.reply_to
        params["status"] = self.send_box.text()
        self.send_box.clear()
        RequestThread(self.main_win.home_stream.get_tweets, self.api.update_status, params, parent=self)

    @dbg
    def got_followed(self, followed):
        self.completer = MultiCompleter(self.send_box, followed)
        self.completer.setModelSorting(QCompleter.CaseInsensitivelySortedModel)
        self.completer.setCaseSensitivity(Qt.CaseInsensitive)
        # used to place the cursor correctly after completion in middle of msg
        self.completer.reset_cursor.connect(self.place_cursor, Qt.QueuedConnection)
        self.send_box.setCompleter(self.completer)

    def set_reply(self, tid):
        self.reply_to = tid

    def set_len(self, text):
        length = len(text)
        # parse URLs which will be wrapped by t.co
        #TODO Tweeter init should make an API request to determine the actual length of t.co URLs
        for i in re.findall(r"http://\S+", text):
            length -= len(i) - 20
        for i in re.findall(r"https://\S+", text):
            length -= len(i) - 21

        if length > 140:
            self.char_count_label.setText('<span style="color:red">{0}</span>'.format(str(length)))
        else:
            self.char_count_label.setText(str(length))
        self.char_count = length
        if length == 0:
            self.reply_to = None

    def place_cursor(self, index):
        # place the cursor at the end of word index
        words = self.send_box.text().split()
        count = len(" ".join(words[:index+1]))
        self.send_box.setCursorPosition(count)


class MainWindow(QMainWindow):
    """Base class for application MainWindow.

    This should be subclassed by individual platforms to add the layout etc."""
    @dbg
    def __init__(self, auth, app, *args):
        QMainWindow.__init__(self, *args)
        self.setWindowTitle("Synchronal")
        self.streams = []
        self.app = app
        self.inited = False
        self.api = tweepy.API(auth, timeout=20, secure=True)
        self.image_requester = ImageRequester()
        self.synch = SynchronalInterface(self)
        self.synch.connected.connect(self.synched)
        self.synch.synch()
        self.home_stream = None

        geometry = QSettings().value("geometry")
        if geometry:
            self.restoreGeometry(geometry)
        self.do_layout()
        self.do_menu()
        self.show()

    @dbg
    def do_initial_streams(self):
        # this should pull the user's standard columns from conf or whatever
        # for now just use home and mentions columns
        new_streams = QSettings().value("streams")
        if new_streams != None:
            new_streams = json.loads(new_streams)
            for i in sorted(new_streams):
                self.add_stream(new_streams[i]["name"])
                rb = self.streams[-1].refresh_box
                rb.setCurrentIndex(rb.findText(new_streams[i]["updates"]))
        else:
            self.add_stream("Home")
            self.add_stream("Mentions")

    @dbg
    def add_stream(self, stream):
        req_kwargs = {"include_entities":True, "include_my_retweet":True}
        if stream[0] == "#":
            # hashtag
            dbg_msg("Hashtag streams not currently supported")
            return
        elif stream[0] == "@":
            # user stream
            req_fn = self.api.user_timeline
            req_kwargs["screen_name"] = stream[1:]
            req_kwargs["include_rts"] = True
        elif stream == "Home":
            # home stream
            req_fn = self.api.home_timeline
        elif stream == "Mentions":
            # user's mentions
            req_fn = self.api.mentions
        else:
            # unrecognised stream type
            dbg_msg("Unrecognised stream.")
            return

        newstream = Stream(stream, req_fn, req_kwargs, self.image_requester, self.synch, self)
        self.stream_layout.addWidget(newstream)
        self.streams.append(newstream)
        if stream == "Home":
            self.home_stream = newstream

    def add_home_stream(self):
        self.add_stream("Home")
    def add_mentions_stream(self):
        self.add_stream("Mentions")
    def add_user_stream(self):
        d = QInputDialog(self)
        d.setInputMode(QInputDialog.TextInput)
        d.setWindowTitle("PyRook")
        d.setLabelText("Username:")
        okay = d.exec_()
        user = d.textValue()
        if (not okay) | (not user):
            return
        if user[0] != "@":
            user = "@" + user
        self.add_stream(user)

    @dbg
    def remove_stream(self, stream):
        self.streams.remove(stream)

    @dbg
    def synched(self, res):
        if res:
            # logged into synchronal
            if not self.inited:
                dbg_msg("Connected to Synchronal Server.")
                self.do_initial_streams()
                self.inited = True
        else:
            dbg_msg("Connection to Synchronal failed.")
            # should handle connection failure at some point

    @dbg
    def closeEvent(self, event):
        settings = QSettings()
        settings.setValue("geometry", self.saveGeometry())
        stream_strs = {}
        count = 0
        for i in self.streams:
            stream_strs[count] = {"name":i.stream_name, "updates":i.refresh_box.currentText()}
            count += 1
        settings.setValue("streams", json.dumps(stream_strs))
        self.app.quit()
        event.accept()

    def do_menu(self):
        datadir = QDesktopServices.storageLocation(QDesktopServices.DataLocation)
        menu = self.menuBar()
        synchronal_menu = menu.addMenu("Synchronal")

        add_menu = synchronal_menu.addMenu("Add Stream")
        add_menu.addAction("Home", self.add_home_stream)
        add_menu.addAction("Mentions", self.add_mentions_stream)
        add_menu.addAction("Username", self.add_user_stream)

        synchronal_menu.addSeparator()

        if debug.debug_enabled:
            synchronal_menu.addAction("Show debug log", debug.debug_widget)

        synchronal_menu.addAction(QApplication.windowIcon(), "About Synchronal", self.about_synchronal)
        synchronal_menu.addAction(QIcon.fromTheme("help-about"), "About Qt", self.about_qt)

        synchronal_menu.addSeparator()

        synchronal_menu.addAction(QIcon.fromTheme(
            "application-exit", QIcon(os.path.join(datadir, "application-exit.png"))),
            "Quit", self.close, QKeySequence("Ctrl+Q"))

    def do_layout(self):
        """This must be redefined in subclasses.

        Needs to, at minimum, provide:
        self.stream_layout with an addWidget function
        self.send_box, a Tweeter."""
        self.stream_layout = QHBoxLayout()
        self.send_box = Tweeter(self.api, self)

    def about_synchronal(self):
        QMessageBox.about(self, "About Synchronal", "Synchronal " + __version__ + "\nA Python/Qt client for Twitter using Synchronal Server.\nhttps://sentynel.com/synchronal\nBy Sam Lade")

    def about_qt(self):
        QMessageBox.aboutQt(self)


class MultiCompleter(QCompleter):
    """A QCompleter which completes individual words, rather than the whole
    contents of a QLineEdit."""
    reset_cursor = Signal(int)
    def __init__(self, tweeter, *args):
        QCompleter.__init__(self, *args)
        self.tweeter = tweeter

    def splitPath(self, path):
        cursor = self.tweeter.cursorPosition()
        words = path.split()
        word = self.get_word(words, cursor)
        return [words[word]]

    def pathFromIndex(self, index):
        # need to return the current text of the item, with the word under the
        # cursor autocompleted.
        cursor = self.tweeter.cursorPosition()
        text = self.tweeter.text()
        words = text.split()
        word = self.get_word(words, cursor)

        words[word] = QCompleter.pathFromIndex(self, index)
        self.reset_cursor.emit(word)
        return " ".join(words)

    def get_word(self, words, cursorpos):
        count = 0
        for i in range(len(words)):
            count += len(words[i])+1
            if count > cursorpos:
                break
        return i


class Wrapping_Label(QLabel):
    """QLabel that wraps words which are too wide."""
    def __init__(self, *args):
        QLabel.__init__(self, *args)
        self.original_text = ""
        self.setWordWrap(True)
        # necessary to allow shrinking which forces wrapping changes
        self.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)

    def setText(self, text):
        self.original_text = text
        self.wrap_text()

    def wrap_text(self):
        text = self.original_text
        # manually wrap long words
        self.msg_width = self.size().width()
        self.font_metric = self.fontMetrics()
        # pull out individual words which are not inside HTML tags
        newmsg = re.sub(r"""(?<!\w\<)\w+(?!\"\>)""", self.break_string, self.original_text)
        QLabel.setText(self, newmsg)

    def break_string(self, match):
        string = match.group()
        width = self.font_metric.width(string)
        #dbg_msg("{} String: {} Widget: {}".format(string, width, self.msg_width))
        if width > self.msg_width:
            # need to break string
            # we assume character widths are all ~average, with a little margin for error
            # refuse to break too short
            step = int(len(string) * 1.0 * self.msg_width / width) - 2
            if step < 10:
                step = 10
            return " ".join(string[i:i+step] for i in range(0, len(string), step))
        return string

    def resizeEvent(self, event):
        self.wrap_text()
        QLabel.resizeEvent(self, event)


class SynchronalInterface(QObject):
    """Provides an interface to the Synchronal server."""
    connected = Signal(bool)
    @dbg
    def synch(self):
        self.api = "http://old.sentynel.com/synchronal/api.py"
        self.reqs = []
        self.valid_sid = False

        settings = QSettings()
        self.user = settings.value("synchronal/user", "")
        self.passw = settings.value("synchronal/pass", "")
        if not self.user:
            # need to log in
            self.login()
        else:
            self.get_sid()

    @dbg
    def login(self):
        win = QDialog()
        layout = QFormLayout()
        win.setLayout(layout)

        layout.addRow(QLabel("Login to Synchronal Server"))

        self.user_box = QLineEdit()
        self.user_box.setMaxLength(32)
        layout.addRow("Username:", self.user_box)

        self.pass_box = QLineEdit()
        self.pass_box.setEchoMode(QLineEdit.Password)
        layout.addRow("Password:", self.pass_box)

        button_layout = QDialogButtonBox(QDialogButtonBox.Cancel)
        layout.addRow(button_layout)
        login_button = QPushButton("Login")
        button_layout.addButton(login_button, QDialogButtonBox.AcceptRole)
        register_button = QPushButton("Register")
        button_layout.addButton(register_button, QDialogButtonBox.AcceptRole)

        button_layout.accepted.connect(win.accept)
        button_layout.rejected.connect(win.reject)
        login_button.clicked.connect(self.set_vals)
        login_button.clicked.connect(self.get_sid)
        register_button.clicked.connect(self.set_vals)
        register_button.clicked.connect(self.register)

        res = win.exec_()
        if not res:
            self.connected.emit(False)

    @dbg
    def set_vals(self):
        self.user = self.user_box.text()
        self.passw = hashlib.sha1(self.pass_box.text()).hexdigest()
        settings = QSettings()
        settings.setValue("synchronal/user", self.user)
        settings.setValue("synchronal/pass", self.passw)

    @dbg
    def register(self):
        self.reg_req = RequestThread(self.registered, urllib2.urlopen, [self.api,
            urllib.urlencode({"method":"register", "user":self.user, "pass":self.passw})])

    @dbg
    def registered(self, req):
        self.reg_req = None
        if req == -1:
            # error
            return
        resp = json.loads(req.read())
        code = resp["synchronal"]["code"]
        if code != 0:
            if code == 11:
                QMessageBox.warning(self.parent(), "Synchronal", "Invalid Synchronal username, please pick another.")
            elif code == 12:
                QMessageBox.warning(self.parent(), "Synchronal", "Synchronal username taken, please pick another.")
            else:
                QMessageBox.warning(self.parent(), "Synchronal", "Server error registering for Synchronal, please retry.")
            self.login()
            return
        # registered successfully
        self.get_sid()

    @dbg
    def get_sid(self):
        self.sid_req = RequestThread(self.got_sid, urllib2.urlopen, [self.api,
            urllib.urlencode({"method":"get_sid", "user":self.user})])

    @dbg
    def got_sid(self, req):
        self.sid_req = None
        if req == -1:
            return
        resp = json.loads(req.read())
        code = resp["synchronal"]["code"]
        if code != 0:
            if code == 21:
                # not registered, attempt to register
                QMessageBox.information(self.parent(), "Synchronal", "Synchronal user not registered. Attempting registration.")
                self.register()
                return
            else:
                QMessageBox.warning(self.parent(), "Synchronal", "Error logging into Synchronal. Try again later.")
                self.connected.emit(False)
                return
        self.sid = resp["synchronal"]["sid"]
        self.valid_sid = True
        dbg_msg("Synchronal SID:", self.sid)
        self.connected.emit(True)

    def get_read(self, stream, slot):
        r = SynRequester(slot, self.api, "get_read", self.user, self.sid, self.passw,
                         stream, None, self.valid_sid, self)
        self.reqs.append(r)

    def set_read(self, stream, slot, tid):
        r = SynRequester(slot, self.api, "set_read", self.user, self.sid, self.passw,
                         stream, str(tid), self.valid_sid, self)
        self.reqs.append(r)

    @dbg
    def sid_expired(self):
        self.valid_sid = False
        for i in self.reqs:
            i.abort()
        try:
            self.connected.connect(self.new_sid, Qt.UniqueConnection)
        except RuntimeError:
            # UniqueConnection raises a RuntimeError on connection fail now, so catch and ignore
            pass
        self.get_sid()

    @dbg
    def new_sid(self, res):
        if not res:
            #crap.
            return
        for i in self.reqs:
            i.retry(self.sid)

    def req_done(self, res, req):
        self.reqs.remove(req)


class SynRequester(QObject):
    """Handler for a single read status request."""
    done = Signal(object, object)
    def __init__(self, slot, api, method, user, sid, passw, stream, tid, start, *args):
        QObject.__init__(self, *args)
        if slot is not None:
            self.done.connect(slot)
        self.done.connect(self.parent().req_done)
        self.started = False
        self.finished = False
        self.aborted = False
        self.retried = False

        self.api = api
        self.method = method
        self.user = user
        self.sid = sid
        self.passw = passw
        self.stream = stream
        self.tid = tid
        if start:
            self.do_req()

    def do_req(self):
        self.started = True
        self.aborted = False
        self.retried = False
        self.finished = False

        h = hashlib.sha1(self.sid + self.passw + self.method + self.stream)
        if self.tid:
            h.update(self.tid)
        h = h.hexdigest()
        params = {"method":self.method, "user":self.user, "hash":h, "stream":self.stream}
        if self.tid:
            params["tid"] = self.tid

        self.req = RequestThread(self.req_done, urllib2.urlopen, [self.api,
            urllib.urlencode(params)])

    def abort(self):
        self.aborted = True

    def retry(self, new_sid = None):
        if new_sid:
            self.sid = new_sid
        self.retried = True
        if not self.started:
            self.do_req()
        elif self.finished:
            self.do_req()

    @dbg
    def req_done(self, req):
        self.req = None
        self.finished = True
        if self.aborted:
            if self.retried:
                self.do_req()
            return

        if req == -1:
            dbg_msg("Request failed")
            self.done.emit(-1, self)
            return
        resp = json.loads(req.read())
        code = resp["synchronal"]["code"]
        if code != 0:
            dbg_msg("Request error, code:", code)
            if code == 31:
                self.parent().sid_expired()
            elif code == 33:
                self.done.emit(-2, self)
            else:
                self.done.emit(-1, self)
            return
        if "tid" in resp["synchronal"]:
            self.done.emit(resp["synchronal"]["tid"], self)
        else:
            self.done.emit(0, self)


class FollowedRequester(QObject):
    """A handler to retrieve a list of all users followed.

    This can be used to supply the username autocompleter."""
    done = Signal(list)

    def __init__(self, slot, api, *args):
        QObject.__init__(self, *args)
        self.api = api
        self.uids = []
        self.users = []
        self.done.connect(slot)
        self.users_req = RequestThread(self.got_users, self.api.friends_ids,
            {"cursor":-1, "stringify_ids":True})

    def got_users(self, uids):
        self.users_req = None
        if uids == -1:
            return
        self.uids.extend(uids[0])
        if uids[1][1] != 0:
            # more uids must be requested
            self.users_req = RequestThread(self.got_users, self.api.friends_ids,
            {"cursor":uids[1][1], "stringify_ids":True})
        else:
            # got all uids, start requesting details
            self.get_details()

    def get_details(self, details=None):
        self.details_request = None
        if details:
            if details == -1:
                # uh oh.
                dbg_msg("Error.")
                return
            for i in details:
                self.users.append(unicode(u"@" + i.screen_name))

        if self.uids:
            batch = self.uids[:100]
            self.uids[:100] = []
            self.details_request = RequestThread(self.get_details, self.api.lookup_users,
                {"user_ids":batch})

        else:
            # want case insensitive completion
            self.users.sort(key=unicode.lower)
            self.done.emit(self.users)


class ImageRequester(QObject):
    """A handler for requesting images.

    Objects can request an image URL from it. It will download and cache the
    image if necessary, and signal the originating object when the image is ready."""
    @dbg
    def __init__(self, *args):
        QObject.__init__(self, *args)

        # dict of image name : done signal
        self.running_requests = {}
        self.requests_queue = collections.OrderedDict()
        self.cache_dir = QDesktopServices.storageLocation(QDesktopServices.CacheLocation)
        c_d = QDir(self.cache_dir)
        if not c_d.exists():
            c_d.mkpath(self.cache_dir)

    def get_image(self, url, reply_slot):
        store = url.replace("/","_").replace(":","_")
        # strip the file extension. Qt uses the extension if present, and this
        # is not guaranteed to be correct. Removing the extension forces
        # detection.
        name, ext = os.path.splitext(store)
        if len(ext) < 6:
            store = name
        store = os.path.join(self.cache_dir, store)
        if os.path.exists(store):
            # file already retrieved
            reply_slot(store)
            return
        if store in self.running_requests:
            # file already being retrieved, connect to done notification
            self.running_requests[store].done.connect(reply_slot)
            return
        if store in self.requests_queue:
            # file queued for retrieval
            self.requests_queue[store].done.connect(reply_slot)
            return
        # need to retrieve the file
        if len(self.running_requests) < 5:
            # perform request immediately
            self.running_requests[store] = ImageRequest(url, self, True)
            self.running_requests[store].done.connect(reply_slot)
            self.running_requests[store].done.connect(self.request_done)
        else:
            # queue request
            self.requests_queue[store] = ImageRequest(url, self, False)
            self.requests_queue[store].done.connect(reply_slot)
            self.requests_queue[store].done.connect(self.request_done)

    def request_done(self, url):
        if url[0] == "~":
            # failed request
            url = url[1:]
        del self.running_requests[url]
        # check for queued requests and begin one if so
        if self.requests_queue:
            store, req = self.requests_queue.popitem(False)
            self.running_requests[store] = req
            req.start()


class ImageRequest(QObject):
    """Handles a single image request.

    Necessary because Qt does not support dynamic signal creation."""
    done = Signal(str)
    def __init__(self, url, requester, start, *args):
        QObject.__init__(self, *args)
        self.requester = requester
        self.url = url
        if start:
            self.start()

    def start(self):
        self.req = RequestThread(self.got_image, urllib2.urlopen, self.url)

    def got_image(self, response):
        url = self.url
        store = url.replace("/","_").replace(":","_")
        name, ext = os.path.splitext(store)
        if len(ext) < 6:
            store = name
        store = os.path.join(self.requester.cache_dir, store)
        if response == -1:
            # request failed, should handle this neatly at some point..
            self.done.emit("~"+store)
            return
        with open(store, "wb") as f:
            f.write(response.read())
        self.done.emit(store)


class RequestThread(QThread):
    """An asynchronous Twitter API request caller.

    Takes the slot to connect completion to, the apifunction (or other
    request) to call, and optionally list/tuple of args, a dict of kwargs,
    or (for convenience) a single item which will be passed in directly."""
    def __init__(self, target, apifunc, params=None, *args, **kwargs):
        QThread.__init__(self, *args, **kwargs)
        self.apifunc = apifunc
        self.params = params
        self.reply = None
        if target != None:
            self.done.connect(target)
        self.dbg.connect(d.thread_debug)
        # ensure that the thread does not signal out before it's finished, to avoid deletion
        # while running crashes
        self.is_finished = False
        self.finished.connect(self.send_reply)
        self.start()

    done = Signal(object)
    dbg = Signal(str, QThread, tuple)

    def run(self):
        self.dbg.emit("start", self, (self.apifunc.__name__, "(", self.params, ")"))
        try:
            if isinstance(self.params, dict):
                req = self.apifunc(**self.params)
            elif isinstance(self.params, (list, tuple)):
                req = self.apifunc(*self.params)
            elif self.params is not None:
                req = self.apifunc(self.params)
            else:
                req = self.apifunc()
        except Exception as err:
            dbg_msg(err)
            self.reply = -1
        else:
            self.reply = req
        self.dbg.emit("msg", self, ("processing done",))
        self.is_finished = True

    def send_reply(self):
        self.dbg.emit("end", self, tuple())
        self.done.emit(self.reply)


class AuthRequester(QObject):
    """Auth to Twitter using OAuth.

    Emits a signal when done containing the auth object to use."""
    done = Signal(object)

    def __init__(self, *args):
        QObject.__init__(self, *args)
        self.auth = tweepy.OAuthHandler("PclSN9AX3Z2q4w229MXZmg", "pjkBoJtmt4toA6aDIxXV7GCXtONEmm3dXDBAgaxEE4", secure=True)

    @dbg
    def get_auth(self):
        settings = QSettings()
        key = settings.value("access_token/key", "")
        secret = settings.value("access_token/secret", "")
        if not key:
            # need to get a key
            dbg_msg("Getting auth from user")
            self.t_auth_url = RequestThread(self.get_pin, self.auth.get_authorization_url)
        else:
            dbg_msg("Already authed, using saved value")
            self.auth.set_access_token(key, secret)
            self.done.emit(self.auth)

    @dbg
    def get_pin(self, authurl):
        self.t_auth_url = None
        if authurl == -1:
            self.done.emit(-1)
            return
        self.auth_url = authurl
        pin_dialog = QDialog()
        layout = QFormLayout()

        auth_button = QPushButton("Get PIN")
        auth_button.clicked.connect(self.open_auth_url)
        layout.addRow(auth_button)

        auth_label = QLabel(authurl.replace("?","?\n"))
        auth_label.setWordWrap(True)
        layout.addRow(auth_label)

        self.pin_box = QLineEdit()
        layout.addRow("PIN:", self.pin_box)

        buttons = QDialogButtonBox(QDialogButtonBox.Ok|QDialogButtonBox.Cancel)
        layout.addRow(buttons)

        pin_dialog.setLayout(layout)

        buttons.accepted.connect(pin_dialog.accept)
        buttons.rejected.connect(pin_dialog.reject)
        res = pin_dialog.exec_()
        if res:
            self.set_pin()
        else:
            self.done.emit(-1)

    @dbg
    def set_pin(self):
        pin = self.pin_box.text()
        self.t_access_token = RequestThread(self.success, self.auth.get_access_token, pin)

    @dbg
    def success(self, access_token):
        self.t_access_token = None
        if access_token == -1:
            self.done.emit(-1)
            return
        dbg_msg(self.auth.access_token.key, self.auth.access_token.secret)
        settings = QSettings()
        settings.setValue("access_token/key", self.auth.access_token.key)
        settings.setValue("access_token/secret", self.auth.access_token.secret)
        self.done.emit(self.auth)

    def open_auth_url(self):
        QDesktopServices.openUrl(QUrl(self.auth_url))
